# RunAI Tutorial

To run jobs on a GPU, EPFL offers multiple clusters. At SaCS, we use the [RCP](https://www.epfl.ch/research/facilities/rcp/rcp-cluster-service-description/) and [IC](https://inside.epfl.ch/ic-it-docs/ic-cluster/) clusters, both of which utilize RunAI for job scheduling.

This guide aims to help new students and lab members quickly set up RunAI and get started efficiently.

For more detailed information on RunAI, there are many publicly available resources, such as the [RCP CaaS documentation](https://wiki.rcp.epfl.ch/en/home/CaaS). In this repository, we provide scripts to automate the setup process, as well as other useful scripts to save you time while working with RunAI. This tutorial currently focuses exclusively on RCP, but we may extend it to cover IC in the future.


## Setting up RunAI

The RunAI Command Line Interface makes monitoring and submitting your jobs to the cluster from the terminal very convenient. The installation process and connection your account are detailed in the [Quick Start guide](https://wiki.rcp.epfl.ch/en/home/CaaS/Quick_Start).

Alternatively, we provide a script that streamlines this process. Currently, the script supports only Linux and macOS. You will be prompted to enter the root password to make the `runai` binary easily accessible to all users on the system. While the script makes the binary available to everyone, only the user who runs the script will be logged into their account. Ensure you don't run the script with sudo in front, as this would make only `root` available to access your RunAI account.
```sh
$ ./scripts/setup_runai/setup.sh --department sacs --cluster rcp
```

### Explanation of the set up process using the provided script

Although it is not essential to read this section if the setup script works as expected, it is highly recommended to do so to gain a better understanding of the setup process and to make any necessary adjustments. The setup script performs several key steps:

1. The `runai` binary is downloaded and placed into a folder where it can be easily accessed through the command line.
2. The Kubernetes configuration file is downloaded, allowing you to interact with the RunAI Kubernetes cluster.
3. You will be prompted to log in to RunAI, associating your user account with the actions you perform using the provided binary.
4. The script will set your personal project as the default prpoject. While this step is generally optional, every RunAI job must be associated with a project. If you don't set a default project, you'll need to include the argument `-p [name of target project]` every time you submit a job. Setting a default project saves you from this extra step, as the target project will be assumed to be your default.

Projects are a crucial aspect of RunAI. In simple terms, every project belongs to a department. A department is a billing unit responsible for all resources assigned to projects within it. In our case, the department is `sacs`. (**Note: The cluster is not free, so avoid leaving jobs running while not doing any work. Even if resources are not actively utilized, they remain assigned to you as long as your job is running, and the department will be billed as if the resources are fully utilized.**) Projects help track where funds are spent. In most cases, you will only have a personal project named `sacs-[your gaspar username]`.



## Additional Resources
Below are some resources to help you use and troubleshoot issues encountered while using the cluster.

### List of Useful Commands
- `runai login` - Connects your runai CLI with your RunAI account and refreshes the OAuth2 token.
- `runai whoami` - Displays the user currently logged in with the `runai` CLI.
- `runai list projects` - Lists the projects you have access to, along with the resources allocated and currently utilized by each project.
- `runai list jobs [-p [project name]]` - Lists the jobs currently running in the specified project.

### Troubleshooting
Here are some common issues you might encounter and their solutions that have worked for us:

- `failed to refresh token: oauth2`: If you encounter the following error after a break from using the runai CLI:
```bash
$ runai list project
Get "https://caas-test.rcp.epfl.ch:443/api?timeout=32s": failed to refresh token: oauth2: cannot fetch token: 400 Bad Request
Response: {"error":"invalid_grant","error_description":"Token is not active"}
```
This error appear to indicate that the OAuth2 token has expired. Logging in again using the `runai login` command will resolve the issue.

<div style="text-align: center; margin-top: 50px;">
    <h2 style="font-size: 2em; color: #FF6347;">Work in Progress</h2>
    <p style="font-size: 1.5em; color: #4682B4;">COMING SOON</p>
</div>

