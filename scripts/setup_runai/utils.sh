#!/bin/bash

# Colors
RED='\033[0;31m'
NC='\033[0m' # No Color

# Print a message in error format and exit
function error_message_and_exit() {
    >&2 echo -e "[${RED}ERROR${NC}] $1"
    exit 1
}

# Parse the setup parameters
function parse_setup_params() {
    # Parameters
    DEFAULT_DEPARTMENT="sacs"
    DEFAULT_CLUSTER="rcp"
    # Print how the script should be used
    function print_usage() {
        echo "Usage: $0 [-d|--department <department_name> (default: $DEFAULT_DEPARTMENT)] [-c|--cluster [rcp] (default: $DEFAULT_CLUSTER)] [-h|--help]"
        exit 1
    }
    # Parse the parameters
    while true; do
        case "$1" in
            -d | --department   ) DEFAULT_DEPARTMENT="$2"; shift 2 ;;
            -c | --cluster      ) DEFAULT_CLUSTER="$2"; shift 2 ;;
            -h | --help         ) print_usage ;;
            * ) break ;;
        esac
    done

    if [ $DEFAULT_CLUSTER != "rcp" ]; then
        error_message_and_exit "Only the cluster 'rcp' is supported at the moment. Exiting."
    fi

    # Make parameters available to the caller
    export DEFAULT_DEPARTMENT, DEFAULT_CLUSTER
}

# Get the first project under a department
function get_first_project_under_department() {
    CHOSEN_DEPARTMENT=$1
    res=$(runai list project | awk -F' {2,}' -v department="$CHOSEN_DEPARTMENT" '$2 == department {print $1; exit}' OFS='\t')
    # Check if the result contains the suffix "(default)" and remove it 
    if [[ "$res" == *"(default)" ]]; then
        res=${res%"(default)"}
        res=$(echo "$res" | xargs)
    fi
    echo $res
}