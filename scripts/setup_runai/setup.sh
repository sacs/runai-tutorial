#!/bin/bash

# Get the directory of the script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Load helper functions
source $SCRIPT_DIR/utils.sh

# Parse the setup parameters
parse_setup_params "$@"

# Download the Run:AI CLI
RUNAI_PATH=/usr/local/bin/runai
if [ "$(uname)" == "Darwin" ]; then     
    DOWNLOAD_LINK="https://rcp-caas-test.rcp.epfl.ch/cli/darwin"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    DOWNLOAD_LINK="https://rcp-caas-test.rcp.epfl.ch/cli/linux"
else
    error_message_and_exit "This script supports only MacOS and Linux. "\
        "Please check https://rcpepfl.run.ai/ as your platform might still be supported by rcp RunAI."
    exit 1
fi
sudo wget --content-disposition $DOWNLOAD_LINK -O $RUNAI_PATH \
    || error_message_and_exit "Can't fetch runai binary. Exittng"
chmod +x $RUNAI_PATH

# Setting up kubeconfig
KUBECONFIG_OUT_DIR=$HOME/.kube
mkdir -p $KUBECONFIG_OUT_DIR
cp $SCRIPT_DIR/rcp_kubeconfig $KUBECONFIG_OUT_DIR/config

# Login to Run:AI
runai login 

# Setup default project
DEFAULT_PROJECT=$(get_first_project_under_department $DEFAULT_DEPARTMENT)
if [ -z "$DEFAULT_PROJECT" ]; then
    echo "No project found under department $DEPARTMENT"
    exit 1
else
    echo "Setting default project to $DEFAULT_PROJECT"
    runai config project $DEFAULT_PROJECT
fi
